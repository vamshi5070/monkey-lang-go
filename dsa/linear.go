package main

import "fmt"

func main(){
	arr := [4]int{23,32,11,100}
	fmt.Println("Arr before linear search: ")
	for i := 0; i < len(arr); i++ {
		fmt.Printf("%d  ",arr[i])
	}

	var key int

	fmt.Print("Enter an integer for search: ")

	_, err := fmt.Scanf("%d", &key)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}
	var flag bool = false
	for i := 0; i < len(arr); i++ {
		if arr[i] == key {
			flag = true
			fmt.Printf("Found key at: %d  ",i)
			break
		} else {
			continue
		}
	}
	if !flag {
		fmt.Printf("Key not found in array ")
	} 
		
	// fmt.Println("TODO: Implement linear for given array of numbers")
}
