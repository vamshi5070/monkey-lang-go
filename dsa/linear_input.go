package main

import "fmt"

func main(){
	var n int
	fmt.Print("Enter the no. of elements: ")
	_, err := fmt.Scanln(&n)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	if n <= 0 {
		fmt.Println("Number of elements should be greater than 0")
		return
	}

	arr := make([]int, n)

	for i := 0; i < n ; i++ {
		fmt.Printf("Enter element %d: ",i+1)
		_, err := fmt.Scanf("%d",&arr[i])
		if err != nil {
			fmt.Println("Error: ", err)
			return
		}
	}

	fmt.Println("Arr before linear search: ")
	for i := 0; i < len(arr); i++ {
		fmt.Printf("%d  ",arr[i])
	}

	var key int

	fmt.Print("Enter the key to search the arr: ")

	_ , err = fmt.Scanln(&key)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	var flag bool = false
	for i := 0; i < len(arr); i++ {
		if arr[i] == key {
			flag = true
			fmt.Printf("Found key at: %d  \n",i)
			break
		} else {
			continue
		}
	}

	if !flag {
		fmt.Printf("Key not found!!! ")
	}
}
		
